/************************************************ 
 *   Orbot detecteur d'obstacle (L293d chip)    *
 ************************************************
 *  Installation : 
 *        - Servo sur pin 10
 *        - SR_04 sur A0 , A1
 *        - Potentiometre sur A2
 *        - Switch sur 2 et NF
 *        - RGB sur A3(R:eteint) , A4(V:marche) , A5(B:obstacle)
 *        - motor A : EN1(3),IN1(4),IN2(5) (DIRECTION)
 *          motor B : EN1(6),IN1(7),IN2(8) (MOTEUR PRINCIPAL)
 *  Note :
 *        - Regler la fonction tourner() suivant le vehicule
 *        - Mieu choisir la vitesse :D
 *        - Verifier que le button est normalement ferme(NF)
 *        - Verifier la led RGB s'elle est bien connecter
 *        - 
 *  Amelioration :
 *        - IR mode par exemple
 *        - piezzo
 *        - photoresistance
 */
 
//Les librairies utilisees
#include <Servo.h>//pour controler les servos moteurs
#include <Ultrasonic.h>

//Pour rendre le code signifiant
#define DISTANCE_OBSTACLE 100 //une distace de 100 cm est considere comme obstacle 
#define V_DIRECTION 255 //La force suffisante pour directionner les roux 0 - 255 
//Angle qu'on va utiliser avec lee servo
#define DEVANT 90
#define DROITE 0
#define GAUCHE 175

//variable contenant la vitesse de marche(potentiometre)
long int vitesseMarche;

//Les broches utilisees
const int Potar_v = 2;//Analog pin 2
const int button = 2;//Digital pin 2
//la led RGB
const int Rouge = A3;
const int Vert = A4;
const int Bleu = A5;

//Variables utilisee globalement
int memButton = HIGH;//utiliser pour se souvenir de l etat du button
int start = 0;//le robot est eteint initialement

//config moteur
//Motor A
int enableA = 3;
int MotorA1 = 4;
int MotorA2 = 5;
//Motor B
int enableB = 6;
int MotorB1 = 7;
int MotorB2 = 8;

//Les objets utilisees
Servo monServo;//Bouger l'oeil du robot
Ultrasonic ultrasonic(A1,A0,12000); // (Trig PIN,Echo PIN,TO) TO de mon SR-04 4cm-206cm

//-----------------------------------------------  Programme Principale

void setup(){
  //Attacher le servo au pin 9 ou 10
  monServo.attach(10);

  //Definitions des broches utilisees
  pinMode(Potar_v,INPUT);//Utilisee pour faire varier la vitesse de marche (un potentiometre)
  pinMode(button,INPUT);//Utiliser pour demarer ou eteindre le robot
  pinMode(Rouge,OUTPUT);
  pinMode(Vert,OUTPUT);
  pinMode(Bleu,OUTPUT);

  //configure pin moteurs
  pinMode (enableA, OUTPUT);
  pinMode (MotorA1, OUTPUT);
  pinMode (MotorA2, OUTPUT);  
  pinMode (enableB, OUTPUT);
  pinMode (MotorB1, OUTPUT);
  pinMode (MotorB2, OUTPUT); 
  
  //Arreter les moteurs
  digitalWrite (enableA, LOW);
  digitalWrite (enableB, LOW);
  
  //Pour le debogage
  Serial.begin(9600);
  //Message de bienvenue
  Serial.println("\t[ Orbot --> Hello :p ]");
  
}

void loop() {
  //====================| Pour l'etat du button
  int etatButton = digitalRead(button);
  //Serial.println(etatButton);
  if( (etatButton != memButton) && (etatButton == LOW) ){
      start = !start;
  }
  memButton = etatButton;
  //=========================================
  
  if(start){//Mise en marche du robot
    
      RGB('v');//allumer la led verte
      
      int mesure;
    
      //Initialiser la vitesse du moteur principale dynamiquement (reglable par potar)
      vitesseMarche = map(analogRead(Potar_v),0,1023,0,255);
    
      Serial.print("la vitesse de marche est :");
      Serial.println(vitesseMarche);
      
      //On prend 5 mesures successives le derniers et selui pris (eviter perturbation seulement)
      for(int i=0;i<5;i++) mesure = ultrasonic.Ranging(CM);
      
      Serial.print("Mesure :");
      Serial.println(mesure);
      
        if(mesure > DISTANCE_OBSTACLE){//Pas d'obstacles
              avancer();
          }else{//Il y a des obstacles
              RGB('b');
              arreter();
              eviter();
          }
    
      Serial.println("-------------------------------------------");
      
      //delay(1000); //pour deboguer seulement sert a ralentir l'execution
        
  }else{
    Serial.println("Je dors ZazzzzzzzzzzzzzzZzzzzzzzZ !!!");
    RGB('r');
    //Arreter les moteurs
    digitalWrite (enableA, LOW);
    digitalWrite (enableB, LOW);
    monServo.write(DEVANT);
  }
}

//-----------------------------------------------  Fin Programme

/*
**************************************************************************
* Cette fonction demarrera le robot en avant sans arret
**************************************************************************
*/
void avancer(){
  Serial.println("J'avance...");
  analogWrite (enableB, vitesseMarche);
  digitalWrite (MotorB1, HIGH);
  digitalWrite (MotorB2, LOW);
}

/*
**************************************************************************
* Cette fonction demarrera le robot en avant sans arret
**************************************************************************
*/
void reculer(){
  Serial.println("Je recule...");
  analogWrite (enableB, vitesseMarche);
  digitalWrite (MotorB1, LOW);
  digitalWrite (MotorB2, HIGH);
}

/*
**************************************************************************
* Cette fonction arretera le robot 
**************************************************************************
*/
void arreter(){
  Serial.println("Je m'arrete...");
  //Pour s'arreter brutement
  reculer();
  delay(300);
  digitalWrite (enableA, LOW);
  digitalWrite (enableB, LOW);
}

/*
**************************************************************************
* Chercher une direction sans obstacle
**************************************************************************
*/
char chercherDirection(){
  bool tourner = true;//sert a inverser le sense de regard
  int mesuredsdirection;//La mesure quand il regardera a une direction
  char directionvide = '1';//On aura le '1' si aucune direction vide n'est trouver 'g' Obstacle a gauche  'd' Obstacle a droite
  int i,j;//servent a boucler

  Serial.println("=>J'analyse les directions (un scan)");
  
  //Regarder a gauche et a droite et choisir la direction vide
  for(j=0;j<2;j++){
    if(tourner){
        monServo.write(GAUCHE);
        delay(300);//attendre le servo pour tourner
        for(i=0;i<5;i++) mesuredsdirection = ultrasonic.Ranging(CM);//la precision seulement
        if(mesuredsdirection > DISTANCE_OBSTACLE){
          directionvide = 'g';//Si pas d'obstacle a gauche
          break;//Si la direction a gauche est vide ce n'est pas la peine pour tourner a droite
        }
      }else{
        monServo.write(DROITE);
        delay(600);//Le double de 300 car ici il va faire un 180 degree
        for(i=0;i<5;i++) mesuredsdirection = ultrasonic.Ranging(CM);
        if(mesuredsdirection > DISTANCE_OBSTACLE) directionvide = 'd';
      }
      tourner = !tourner;
  }
      
  monServo.write(DEVANT);
  //S'il ne trouve pas de direction il recule un peu est essai de trouver une de nouveau
  if(directionvide == '1'){
    Serial.println("mmm! Pas de direction vide, Je vais reculer...");
    reculer();
    delay(1000);
    }
    
  return directionvide;
}

/*
**************************************************************************
* Tourner a une direction determiner 
* Droite ou Gauche
**************************************************************************
*/
void tourner(char c){
  analogWrite (enableA, V_DIRECTION);
  if(c == 'g'){//Tourner a gauche
    Serial.println("Je vais tourner a gauche...");
    //Tourner a droite
    digitalWrite (MotorA1, LOW);
    digitalWrite (MotorA2, HIGH);
    reculer();
    delay(1500);
    //Tourner a gauche
    digitalWrite (MotorA1, HIGH);
    digitalWrite (MotorA2, LOW);
    avancer();
    delay(1500);
    //eteindre moteur principal
    digitalWrite (enableB, LOW);
    
  }else if(c == 'd'){//Tourner a droite
    Serial.println("Je vais tourner a droite...");
    //Tourner a gauche
    digitalWrite (MotorA1, HIGH);
    digitalWrite (MotorA2, LOW);
    reculer();
    delay(1500);
    //Tourner a droite
    digitalWrite (MotorA1, LOW);
    digitalWrite (MotorA2, HIGH);
    avancer();
    delay(1500);
    //eteindre moteur principal
    digitalWrite (enableB, LOW);
  }

  digitalWrite (enableA, LOW);
}
/*
**************************************************************************
* Essayer d'eviter l'obstacle
**************************************************************************
*/
int eviter(){
  Serial.println("*** Obstacle detecter :s en train de l'eviter ***");
  char directiontrouver = chercherDirection();
  Serial.print("Direction vide trouver :");
  Serial.println(directiontrouver);
 
  tourner(directiontrouver);
}

/*
**************************************************************************
* Allumer une diode RGB
**************************************************************************
*/
void RGB(char c) {
  if(c == 'r'){
  digitalWrite(Rouge,HIGH);
  digitalWrite(Vert,LOW);
  digitalWrite(Bleu,LOW);
  }else if(c == 'v'){
  digitalWrite(Rouge,LOW);
  digitalWrite(Vert,HIGH);
  digitalWrite(Bleu,LOW);
  }else if(c == 'b'){
  digitalWrite(Rouge,LOW);
  digitalWrite(Vert,LOW);
  digitalWrite(Bleu,HIGH);
  }
}
